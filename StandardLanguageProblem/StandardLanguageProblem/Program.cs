﻿using StandardLanguageProblem.Services;
using System;

namespace StandardLanguageProblem
{
    class Program
    {
        static void Main(string[] args)
        {
            InputService inputService = new InputService();

            while (true)
            {
                string input = Console.ReadLine();
                string output = inputService.ReadInput(input);

                if(output == "exit")
                {
                    Environment.Exit(0);
                }
                else
                {
                    Console.WriteLine(output);
                }
            }
        }
    }
}
