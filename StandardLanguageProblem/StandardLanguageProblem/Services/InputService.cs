﻿using StandardLanguageProblem.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StandardLanguageProblem.Services
{
    public class InputService
    {
        private Atm atm;

        public InputService()
        {
            atm = new Atm();
        }

        public string ReadInput(string inputString)
        {
            string[] inputArray = inputString.Split();
            string inputType = inputArray[0];
            string output = string.Empty;
            StringBuilder sb = new StringBuilder();

            try
            {
                switch (inputType.ToLower())
                {
                    case "r":
                        atm.Restock();
                        sb.AppendLine("Machine balance:");
                        output += sb.ToString();
                        output += atm.GetBills();
                        break;
                    case "w":
                        int valueToWidthdraw = Int32.Parse(inputArray[1].Split('$')[1]);

                        if (Withdraw(valueToWidthdraw))
                        {
                            sb.AppendLine(string.Format("Success: Dispensed ${0}", valueToWidthdraw));
                            sb.AppendLine("Machine balance:");
                            sb.Append(atm.GetBills());
                        }
                        else
                        {
                            sb.AppendLine("Failure: insufficient funds");
                        }

                        output += sb.ToString();
                        break;
                    case "i":
                        var denominations = InquireDenominations(inputArray);
                        output = atm.GetNumberOfBillsForDenomination(denominations);
                        break;
                    case "q":
                        output = "exit";
                        break;
                    default:
                        output = "Failure: Invalid Command";
                        break;
                }
            }
            catch (Exception ex)
            {
                output = "Failure: Invalid Command";
            }

            return output;
        }

        private bool Withdraw(int value)
        {
            var runningTotal = value;
            int numDenominationToRemove = 0;
            var cachedAtmObject = new Atm(atm);

            foreach (var denomination in atm.Denominations)
            {
                numDenominationToRemove = runningTotal / denomination.DenomiationValue;

                if (!denomination.RemoveBills(numDenominationToRemove))
                {
                    break;
                }

                runningTotal -= (numDenominationToRemove * denomination.DenomiationValue);
            }

            if(runningTotal > 0)
            {
                atm = (Atm)cachedAtmObject;
                return false;
            }
            else
            {
                return true;
            }

        }

        private List<Denomination> InquireDenominations(string[] inputArray)
        {
            List<Denomination> denominations = new List<Denomination>();

            for(var i = 1; i < inputArray.Length; i++)
            {
                int value = Int32.Parse(inputArray[i].Split('$')[1]);
                Denomination denomination = atm.Denominations.FirstOrDefault(x => x.DenomiationValue == value);

                if(denomination != null)
                {
                    denominations.Add(denomination);
                }
            }

            return denominations;
        }
    }
}
