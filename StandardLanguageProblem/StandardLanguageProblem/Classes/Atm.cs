﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StandardLanguageProblem.Classes
{
    public class Atm
    {
        private Denomination denomiation100Dollar;
        private Denomination denomiation50Dollar;
        private Denomination denomiation20Dollar;
        private Denomination denomiation10Dollar;
        private Denomination denomiation5Dollar;
        private Denomination denomiation1Dollar;

        public List<Denomination> Denominations;

        public Atm()
        {
            denomiation100Dollar = new Denomination(100);
            denomiation50Dollar = new Denomination(50);
            denomiation20Dollar = new Denomination(20);
            denomiation10Dollar = new Denomination(10);
            denomiation5Dollar = new Denomination(5);
            denomiation1Dollar = new Denomination(1);

            Denominations = new List<Denomination>
            {
                denomiation100Dollar,
                denomiation50Dollar,
                denomiation20Dollar,
                denomiation10Dollar,
                denomiation5Dollar,
                denomiation1Dollar
            };
        }

        public Atm(Atm other)
        {
            denomiation100Dollar = (Denomination)other.denomiation100Dollar.Clone();
            denomiation50Dollar = (Denomination)other.denomiation50Dollar.Clone();
            denomiation20Dollar = (Denomination)other.denomiation20Dollar.Clone();
            denomiation10Dollar = (Denomination)other.denomiation10Dollar.Clone();
            denomiation5Dollar = (Denomination)other.denomiation5Dollar.Clone();
            denomiation1Dollar = (Denomination)other.denomiation1Dollar.Clone();

            Denominations = new List<Denomination>
            {
                denomiation100Dollar,
                denomiation50Dollar,
                denomiation20Dollar,
                denomiation10Dollar,
                denomiation5Dollar,
                denomiation1Dollar
            };
        }

        public void Restock()
        {
            denomiation100Dollar.Restock();
            denomiation50Dollar.Restock();
            denomiation20Dollar.Restock();
            denomiation10Dollar.Restock();
            denomiation5Dollar.Restock();
            denomiation1Dollar.Restock();
        }

        public string GetBills()
        {
            return GetBillsHelper(this.Denominations);
        }

        public string GetNumberOfBillsForDenomination(List<Denomination> denominations)
        {
            return GetBillsHelper(denominations);
        }

        private string GetBillsHelper(List<Denomination> denominations)
        {
            StringBuilder sb = new StringBuilder();

            foreach (var denomination in denominations)
            {
                sb.Append(denomination.ToString());
                sb.AppendLine();
            }

            return sb.ToString();
        }
    }
}