﻿using System;

namespace StandardLanguageProblem.Classes
{
    public class Denomination : ICloneable
    {
        public int DenomiationValue { get; }
        public int NumberOfBills { get; private set; }


        public Denomination (int value)
        {
            DenomiationValue = value;
            Restock();
        }

        public void Restock()
        {
            NumberOfBills = 10;
        }

        public bool RemoveBills(int numToRemove)
        {
            if((NumberOfBills - numToRemove) < 0)
            {
                return false;
            }
            else
            {
                NumberOfBills -= numToRemove;
                return true;
            }
        }

        public override string ToString()
        {
            return string.Format("${0} - {1}", DenomiationValue, NumberOfBills);
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
