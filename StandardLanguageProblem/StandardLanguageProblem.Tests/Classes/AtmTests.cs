﻿using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using StandardLanguageProblem.Classes;

namespace StandardLanguageProblem.Tests.Classes
{
    [TestClass]
    public class AtmTests
    {
        [TestMethod]
        public void AtmNewObjectTest()
        {
            Atm atm = new Atm();
            foreach (var denomination in atm.Denominations)
            {
                Assert.AreEqual(10, denomination.NumberOfBills);
            }
        }

        [TestMethod]
        public void AtmNewObjectOverridConstructorTest()
        {
            Atm atmChangedValues = new Atm();
            var denominations = atmChangedValues.Denominations;
            denominations.Single(x => x.DenomiationValue == 100).RemoveBills(2);
            denominations.Single(x => x.DenomiationValue == 20).RemoveBills(5);
            denominations.Single(x => x.DenomiationValue == 1).RemoveBills(9);

            Atm atm = new Atm(atmChangedValues);

            Assert.AreEqual(8, atm.Denominations.First(x => x.DenomiationValue == 100).NumberOfBills);
            Assert.AreEqual(10, atm.Denominations.First(x => x.DenomiationValue == 50).NumberOfBills);
            Assert.AreEqual(5, atm.Denominations.First(x => x.DenomiationValue == 20).NumberOfBills);
            Assert.AreEqual(10, atm.Denominations.First(x => x.DenomiationValue == 10).NumberOfBills);
            Assert.AreEqual(10, atm.Denominations.First(x => x.DenomiationValue == 5).NumberOfBills);
            Assert.AreEqual(1, atm.Denominations.First(x => x.DenomiationValue == 1).NumberOfBills);
        }

        [TestMethod]
        public void AtmRestockTest()
        {
            Atm atm = new Atm();
            var denominations = atm.Denominations;
            denominations.Single(x => x.DenomiationValue == 100).RemoveBills(2);
            denominations.Single(x => x.DenomiationValue == 50).RemoveBills(10);
            denominations.Single(x => x.DenomiationValue == 20).RemoveBills(9);
            denominations.Single(x => x.DenomiationValue == 10).RemoveBills(5);
            denominations.Single(x => x.DenomiationValue == 1).RemoveBills(1);

            Assert.AreEqual(8, denominations.First(x => x.DenomiationValue == 100).NumberOfBills);
            Assert.AreEqual(0, denominations.First(x => x.DenomiationValue == 50).NumberOfBills);
            Assert.AreEqual(1, denominations.First(x => x.DenomiationValue == 20).NumberOfBills);
            Assert.AreEqual(5, denominations.First(x => x.DenomiationValue == 10).NumberOfBills);
            Assert.AreEqual(10, denominations.First(x => x.DenomiationValue == 5).NumberOfBills);
            Assert.AreEqual(9, denominations.First(x => x.DenomiationValue == 1).NumberOfBills);

            atm.Restock();

            Assert.AreEqual(10, denominations.First(x => x.DenomiationValue == 100).NumberOfBills);
            Assert.AreEqual(10, denominations.First(x => x.DenomiationValue == 50).NumberOfBills);
            Assert.AreEqual(10, denominations.First(x => x.DenomiationValue == 20).NumberOfBills);
            Assert.AreEqual(10, denominations.First(x => x.DenomiationValue == 10).NumberOfBills);
            Assert.AreEqual(10, denominations.First(x => x.DenomiationValue == 5).NumberOfBills);
            Assert.AreEqual(10, denominations.First(x => x.DenomiationValue == 1).NumberOfBills);
        }

        [TestMethod]
        public void AtmGetDenominationsNoChangesTest()
        {
            Atm atm = new Atm();
            var denominations = atm.Denominations;

            StringBuilder sb = new StringBuilder();

            foreach (var denomination in denominations)
            {
                sb.Append(denomination.ToString());
                sb.AppendLine();
            }

            Assert.AreEqual(sb.ToString(), atm.GetNumberOfBillsForDenomination(denominations));
        }

        [TestMethod]
        public void AtmGetDenominationsWithChangesTest()
        {
            Atm atm = new Atm();
            var denominations = atm.Denominations;
            denominations.Single(x => x.DenomiationValue == 100).RemoveBills(2);
            denominations.Single(x => x.DenomiationValue == 50).RemoveBills(10);
            denominations.Single(x => x.DenomiationValue == 20).RemoveBills(9);
            denominations.Single(x => x.DenomiationValue == 10).RemoveBills(5);
            denominations.Single(x => x.DenomiationValue == 1).RemoveBills(1);

            StringBuilder sb = new StringBuilder();

            foreach (var denomination in denominations)
            {
                sb.Append(denomination.ToString());
                sb.AppendLine();
            }

            Assert.AreEqual(sb.ToString(), atm.GetNumberOfBillsForDenomination(denominations));
        }

        [TestMethod]
        public void AtmGetBillsTest()
        {
            Atm atm = new Atm();
            var denominations = atm.Denominations;
            StringBuilder sb = new StringBuilder();

            foreach (var denomination in denominations)
            {
                sb.Append(denomination.ToString());
                sb.AppendLine();
            }

            Assert.AreEqual(sb.ToString(), atm.GetBills());
        }

        [TestMethod]
        public void AtmGetBillsWithChangesTest()
        {
            Atm atm = new Atm();
            var denominations = atm.Denominations;
            denominations.Single(x => x.DenomiationValue == 100).RemoveBills(2);
            denominations.Single(x => x.DenomiationValue == 50).RemoveBills(10);
            denominations.Single(x => x.DenomiationValue == 20).RemoveBills(9);
            denominations.Single(x => x.DenomiationValue == 10).RemoveBills(5);
            denominations.Single(x => x.DenomiationValue == 1).RemoveBills(1);

            StringBuilder sb = new StringBuilder();

            foreach (var denomination in denominations)
            {
                sb.Append(denomination.ToString());
                sb.AppendLine();
            }

            Assert.AreEqual(sb.ToString(), atm.GetBills());
        }
    }
}
