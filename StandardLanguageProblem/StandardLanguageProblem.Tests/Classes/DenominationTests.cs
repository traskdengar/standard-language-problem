﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StandardLanguageProblem.Classes;

namespace StandardLanguageProblem.Tests.Classes
{
    [TestClass]
    public class DenominationTests
    {

        [TestMethod]
        public void DenominationCreateTest()
        {
            Denomination denomination = new Denomination(100);

            Assert.AreEqual(100, denomination.DenomiationValue);
            Assert.AreEqual(10, denomination.NumberOfBills);
        }

        [TestMethod]
        public void DenominationRemoveBillTest()
        {
            Denomination denomination = new Denomination(100);
            Assert.AreEqual(10, denomination.NumberOfBills);

            bool result = denomination.RemoveBills(5);

            Assert.IsTrue(result);
            Assert.AreEqual(5, denomination.NumberOfBills);
        }

        [TestMethod]
        public void DenominationRemoveBillToZeroTest()
        {
            Denomination denomination = new Denomination(100);
            Assert.AreEqual(10, denomination.NumberOfBills);

           bool result = denomination.RemoveBills(10);

            Assert.IsTrue(result);
            Assert.AreEqual(0, denomination.NumberOfBills);
        }

        [TestMethod]
        public void DenominationRemoveBillBelowZeroTest()
        {
            Denomination denomination = new Denomination(100);
            Assert.AreEqual(10, denomination.NumberOfBills);

            bool result = denomination.RemoveBills(8);

            Assert.IsTrue(result);
            Assert.AreEqual(2, denomination.NumberOfBills);

            result = denomination.RemoveBills(3);
            Assert.IsFalse(result);
            Assert.AreEqual(2, denomination.NumberOfBills);
        }

        [TestMethod]
        public void DenominationRestockTest()
        {
            Denomination denomination = new Denomination(100);

            denomination.RemoveBills(5);
            Assert.AreEqual(5, denomination.NumberOfBills);

            denomination.Restock();
            Assert.AreEqual(10, denomination.NumberOfBills);
        }

        [TestMethod]
        public void DenominationToStringTest()
        {
            Denomination denomination = new Denomination(100);
            string output = string.Format("${0} - {1}", denomination.DenomiationValue, denomination.NumberOfBills);

            Assert.AreEqual(output, denomination.ToString());
        }
    }
}
