﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StandardLanguageProblem.Services;
using System.Text;

namespace StandardLanguageProblem.Tests
{
    [TestClass]
    public class InputServiceTests
    {
        #region Restock Tests
        [TestMethod]
        public void InputReadRestockInputTest()
        {
            InputService inputService = new InputService();
            string response = inputService.ReadInput("R");

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Machine balance:");
            sb.AppendLine("$100 - 10");
            sb.AppendLine("$50 - 10");
            sb.AppendLine("$20 - 10");
            sb.AppendLine("$10 - 10");
            sb.AppendLine("$5 - 10");
            sb.AppendLine("$1 - 10");

            Assert.IsFalse(string.IsNullOrEmpty(response));
            Assert.AreEqual(sb.ToString(), response);
        }

        [TestMethod]
        public void InputReadRestockLowerCaseInputTest()
        {
            InputService inputService = new InputService();
            string response = inputService.ReadInput("r");

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Machine balance:");
            sb.AppendLine("$100 - 10");
            sb.AppendLine("$50 - 10");
            sb.AppendLine("$20 - 10");
            sb.AppendLine("$10 - 10");
            sb.AppendLine("$5 - 10");
            sb.AppendLine("$1 - 10");

            Assert.IsFalse(string.IsNullOrEmpty(response));
            Assert.AreEqual(sb.ToString(), response);
        }

        [TestMethod]
        public void InputReadRestockAfterWithdrawlInputTest()
        {
            InputService inputService = new InputService();
            inputService.ReadInput("w $208");
            string response = inputService.ReadInput("R");

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Machine balance:");
            sb.AppendLine("$100 - 10");
            sb.AppendLine("$50 - 10");
            sb.AppendLine("$20 - 10");
            sb.AppendLine("$10 - 10");
            sb.AppendLine("$5 - 10");
            sb.AppendLine("$1 - 10");

            Assert.IsFalse(string.IsNullOrEmpty(response));
            Assert.AreEqual(sb.ToString(), response);
        }
        #endregion

        #region Withdrawl Tests
        [TestMethod]
        public void InputReadWithdrawlInputTest()
        {
            InputService inputService = new InputService();
            string response = inputService.ReadInput("w $208");

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Success: Dispensed $208");
            sb.AppendLine("Machine balance:");
            sb.AppendLine("$100 - 8");
            sb.AppendLine("$50 - 10");
            sb.AppendLine("$20 - 10");
            sb.AppendLine("$10 - 10");
            sb.AppendLine("$5 - 9");
            sb.AppendLine("$1 - 7");

            Assert.IsFalse(string.IsNullOrEmpty(response));
            Assert.AreEqual(sb.ToString(), response);
        }

        [TestMethod]
        public void InputReadWithdrawlInputMultipleWithdrawlsTest()
        {
            InputService inputService = new InputService();
            string response = inputService.ReadInput("w $208");

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Success: Dispensed $208");
            sb.AppendLine("Machine balance:");
            sb.AppendLine("$100 - 8");
            sb.AppendLine("$50 - 10");
            sb.AppendLine("$20 - 10");
            sb.AppendLine("$10 - 10");
            sb.AppendLine("$5 - 9");
            sb.AppendLine("$1 - 7");

            Assert.IsFalse(string.IsNullOrEmpty(response));
            Assert.AreEqual(sb.ToString(), response);

            response = inputService.ReadInput("w $9");

            sb.Clear();
            sb.AppendLine("Success: Dispensed $9");
            sb.AppendLine("Machine balance:");
            sb.AppendLine("$100 - 8");
            sb.AppendLine("$50 - 10");
            sb.AppendLine("$20 - 10");
            sb.AppendLine("$10 - 10");
            sb.AppendLine("$5 - 8");
            sb.AppendLine("$1 - 3");

            Assert.IsFalse(string.IsNullOrEmpty(response));
            Assert.AreEqual(sb.ToString(), response);
        }

        [TestMethod]
        public void InputReadWithdrawlInputInsufficentFundsTest()
        {
            InputService inputService = new InputService();
            string response = inputService.ReadInput("w $1000000");

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Failure: insufficient funds");

            Assert.IsFalse(string.IsNullOrEmpty(response));
            Assert.AreEqual(sb.ToString(), response);
        }

        [TestMethod]
        public void InputReadWithdrawlInputMultipleWithdrawlsInsufficentFundsTest()
        {
            InputService inputService = new InputService();
            string response = inputService.ReadInput("w $208");

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Success: Dispensed $208");
            sb.AppendLine("Machine balance:");
            sb.AppendLine("$100 - 8");
            sb.AppendLine("$50 - 10");
            sb.AppendLine("$20 - 10");
            sb.AppendLine("$10 - 10");
            sb.AppendLine("$5 - 9");
            sb.AppendLine("$1 - 7");

            Assert.IsFalse(string.IsNullOrEmpty(response));
            Assert.AreEqual(sb.ToString(), response);

            response = inputService.ReadInput("w $9");

            sb.Clear();
            sb.AppendLine("Success: Dispensed $9");
            sb.AppendLine("Machine balance:");
            sb.AppendLine("$100 - 8");
            sb.AppendLine("$50 - 10");
            sb.AppendLine("$20 - 10");
            sb.AppendLine("$10 - 10");
            sb.AppendLine("$5 - 8");
            sb.AppendLine("$1 - 3");

            Assert.IsFalse(string.IsNullOrEmpty(response));
            Assert.AreEqual(sb.ToString(), response);

            response = inputService.ReadInput("w $9");
            sb.Clear();
            sb.AppendLine("Failure: insufficient funds");

            Assert.IsFalse(string.IsNullOrEmpty(response));
            Assert.AreEqual(sb.ToString(), response);
        }

        [TestMethod]
        public void InputReadWithdrawlInputBadInputTest()
        {
            InputService inputService = new InputService();
            string response = inputService.ReadInput("w $ab");

            StringBuilder sb = new StringBuilder();
            sb.Append("Failure: Invalid Command");

            Assert.IsFalse(string.IsNullOrEmpty(response));
            Assert.AreEqual(sb.ToString(), response);
        }

        [TestMethod]
        public void InputReadWithdrawlInputBadInputNoDollarSignTest()
        {
            InputService inputService = new InputService();
            string response = inputService.ReadInput("w 100");

            StringBuilder sb = new StringBuilder();
            sb.Append("Failure: Invalid Command");

            Assert.IsFalse(string.IsNullOrEmpty(response));
            Assert.AreEqual(sb.ToString(), response);
        }

        [TestMethod]
        public void InputReadWithdrawlInputNoInputTest()
        {
            InputService inputService = new InputService();
            string response = inputService.ReadInput("w ");

            StringBuilder sb = new StringBuilder();
            sb.Append("Failure: Invalid Command");

            Assert.IsFalse(string.IsNullOrEmpty(response));
            Assert.AreEqual(sb.ToString(), response);
        }
        #endregion

        #region Inspect Tests
        [TestMethod]
        public void InputReadInspectTest()
        {
            InputService inputService = new InputService();
            string response = inputService.ReadInput("i $20");

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("$20 - 10");

            Assert.IsFalse(string.IsNullOrEmpty(response));
            Assert.AreEqual(sb.ToString(), response);
        }

        [TestMethod]
        public void InputReadInspectMultipleDenominationsTest()
        {
            InputService inputService = new InputService();
            string response = inputService.ReadInput("i $20 $5");

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("$20 - 10");
            sb.AppendLine("$5 - 10");

            Assert.IsFalse(string.IsNullOrEmpty(response));
            Assert.AreEqual(sb.ToString(), response);
        }

        [TestMethod]
        public void InputReadInspectMultipleDenominationsAfterWithdrawlTest()
        {
            InputService inputService = new InputService();
            string response = inputService.ReadInput("w $208");

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Success: Dispensed $208");
            sb.AppendLine("Machine balance:");
            sb.AppendLine("$100 - 8");
            sb.AppendLine("$50 - 10");
            sb.AppendLine("$20 - 10");
            sb.AppendLine("$10 - 10");
            sb.AppendLine("$5 - 9");
            sb.AppendLine("$1 - 7");

            Assert.IsFalse(string.IsNullOrEmpty(response));
            Assert.AreEqual(sb.ToString(), response);

            response = inputService.ReadInput("i $100 $50 $5 $1");

            sb.Clear();
            sb.AppendLine("$100 - 8");
            sb.AppendLine("$50 - 10");
            sb.AppendLine("$5 - 9");
            sb.AppendLine("$1 - 7");

            Assert.IsFalse(string.IsNullOrEmpty(response));
            Assert.AreEqual(sb.ToString(), response);
        }

        [TestMethod]
        public void InputReadInspectAllDenominationsTest()
        {
            InputService inputService = new InputService();
            string response = inputService.ReadInput("i $100 $50 $20 $10 $5 $1");

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("$100 - 10");
            sb.AppendLine("$50 - 10");
            sb.AppendLine("$20 - 10");
            sb.AppendLine("$10 - 10");
            sb.AppendLine("$5 - 10");
            sb.AppendLine("$1 - 10");

            Assert.IsFalse(string.IsNullOrEmpty(response));
            Assert.AreEqual(sb.ToString(), response);
        }

        [TestMethod]
        public void InputReadInspectNoDenominationsTest()
        {
            InputService inputService = new InputService();
            string response = inputService.ReadInput("i");

            Assert.IsTrue(string.IsNullOrEmpty(response));
        }
        #endregion

        #region Quit Tests
        [TestMethod]
        public void InputReadQuitTest()
        {
            InputService inputService = new InputService();
            string response = inputService.ReadInput("q");

            StringBuilder sb = new StringBuilder();
            sb.Append("exit");

            Assert.IsFalse(string.IsNullOrEmpty(response));
            Assert.AreEqual(sb.ToString(), response);
        }
        #endregion

        #region Invalid Input Tests
        [TestMethod]
        public void InputReadInvalidInputTest()
        {
            InputService inputService = new InputService();
            string response = inputService.ReadInput("k");

            StringBuilder sb = new StringBuilder();
            sb.Append("Failure: Invalid Command");

            Assert.IsFalse(string.IsNullOrEmpty(response));
            Assert.AreEqual(sb.ToString(), response);
        }

        [TestMethod]
        public void InputReadInvalidNoInputTest()
        {
            InputService inputService = new InputService();
            string response = inputService.ReadInput("");

            StringBuilder sb = new StringBuilder();
            sb.Append("Failure: Invalid Command");

            Assert.IsFalse(string.IsNullOrEmpty(response));
            Assert.AreEqual(sb.ToString(), response);
        }
        #endregion
    }
}
